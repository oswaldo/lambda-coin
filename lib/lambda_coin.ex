defmodule LambdaCoin do
  def append_to_blockchain(data, blockchain) do
    block_gen_fn = fn(previous_block, data) ->
      :crypto.hash(:sha256,
        [Integer.to_string(:os.system_time(:seconds)), data, previous_block]
      ) |> Base.encode16
    end

    if (length(blockchain) == 0) do
      [block_gen_fn.('!', 'genesis block')]
    else
      blockchain ++ [block_gen_fn.(List.last(blockchain), data)]
    end
  end
end
