defmodule LambdaCoinTest do
  use ExUnit.Case
  doctest LambdaCoin

  test 'returns genesis block if empty blocks list is given' do
    assert length(LambdaCoin.append_to_blockchain('first block!', [])) == 1
  end

  test 'returns blockchain with appended block when called multiple times' do
    blockchain = ['hash123', 'hash321']

    appended_blockchain = LambdaCoin.append_to_blockchain('new block data', blockchain)

    assert length(appended_blockchain) == 3
  end
end
